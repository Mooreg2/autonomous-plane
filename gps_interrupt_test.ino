
#include <Servo.h>
#include <TinyGPS++.h>

TinyGPSPlus gps;

void setup() {
  Serial.begin(9600);
  while(!Serial);
  Serial.println("Starting GPS interrupt test.");
  
  Serial1.begin(9600); //connection for GPS (using Feather + Feather Wing GPS)
  
    while (!gps.location.isValid() || !gps.location.isUpdated() || gps.hdop.value() == 0.0 || gps.satellites.value() < 3) {
      while (Serial1.available() > 0) {
        gps.encode(Serial1.read());
      }
    }
    Serial.print(gps.satellites.value());
    Serial.print(" hdop = ");
    Serial.println((double) gps.hdop.value() / 100.0);
    
    // Starts interupts
    OCR0A = 0xAF;
    TIMSK0 |= _BV(OCIE0A);

}

void loop(void) {
  Serial.print(gps.hdop.value());
  Serial.print(" ");
  Serial.print(gps.location.isUpdated());
  Serial.print(" ");
  Serial.print(gps.location.lat(), 8);
  Serial.print(" ");
  Serial.println(gps.location.lng(), 8);
}

// Executes when interupt is called
SIGNAL(TIMER0_COMPA_vect) {
  // Do not block! Just read any GPS text that is available.
  while (Serial1.available() > 0) {
    gps.encode(Serial1.read());
  }
}