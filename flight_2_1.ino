#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Servo.h>
#include <TinyGPS++.h>

//#define SERIAL_MONITOR 1

//pin assignments
const int LED_PIN = 13;
const int ESC_PIN = 9;
const int LEFT_ELEVON_PIN = 10;
const int RIGHT_ELEVON_PIN = 11;

//sensors
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);
TinyGPSPlus gps;

//servos (esc = electronic speed control)
Servo leftElevon, rightElevon, esc;

//limits
const int ESC_MIN_MICROSECONDS = 1000;
const int ESC_MAX_MICROSECONDS = 2000;
const double ACCEL_CALIBRATION_LIMIT = 0.20;

const long TIME_LIMIT_MILLISECONDS = 11000;
const long TIME_LIMIT_TAKEOFF = 3000;
const long TIME_LIMIT_LANDING = 6000;
const long TIME_LIMIT_MONITOR = 10 * 1000;
const double DISTANCE_LIMIT_METERS = 50;

const double MIN_OPERATIONAL_ALTITUDE = 10.0;
const double SERVO_LIMIT_MIN = 40.0;
const double SERVO_LIMIT_MAX = 140.0;
const double SERVO_INTERVAL = (SERVO_LIMIT_MAX - SERVO_LIMIT_MIN) / 2.0; // represents maximum motion from center to either extreme

//calibration -- change these values until elevon are even / zero pitch
const double LEFT_ELEVON_CORRECTION = 20.0;
const double RIGHT_ELEVON_CORRECTION = 0.0;

//most recent event
sensors_event_t event;

//global state variables -- set once, during pre-flight
long startTime;
long monitorTime;
double pitchSetting = 0.0;
double rollSetting = 0.0;
double initialAlt;
double initialLat;
double initialLng;
double initialPitch;
double initialRoll;

//global state variables -- updated continously
double correctedPitch;
//double targetPitch;

double correctedRoll;
//double targetRoll;

double correctedAlt;
double currentLat;
double currentLng;
double currentDistance;
double targetAlt;

boolean landing = false;
boolean takeoff = false;

void setup() {

  #ifdef SERIAL_MONITOR
    Serial.begin(9600);
    while (!Serial);
    Serial.println("autonmous aircraft 1.0");
  #endif
  
  Serial1.begin(9600); //connection for GPS (using Feather + Feather Wing GPS)
  
  pinMode(LED_PIN, OUTPUT);  
  leftElevon.attach(LEFT_ELEVON_PIN);
  rightElevon.attach(RIGHT_ELEVON_PIN);
  esc.attach(ESC_PIN);

  //pre-emptive setting in case ESC is armed
  setThrottle(0);

  //center the control surfaces
  setPitchAndRoll(0.0, 0.0);
  delay(1000);

  // countdown
  flashDelay(750, 10);
  
  // initialize and test all subsystems
  initializeAccelerometer();
  //initializeGPS();
  wait();
  calibratePitch();
  wait();
  calibrateRoll();
  wait();
  preflightControlCheck();
  wait();
  armESC();

  // countdown
  flashDelay(750, 10);
  
  // trap start time
  startTime = millis();
  monitorTime = millis();

  takeoff = true;
}

void initializeAccelerometer() {
  if (!accel.begin()) {
    #ifdef SERIAL_MONITOR
      Serial.println("Accelerometer is missing or disconnected.");
    #endif
    abortFlight();
  }
}

void initializeGPS() {
    
    #ifdef SERIAL_MONITOR
      Serial.println("Initializing GPS");
    #endif
    
    while (!gps.location.isValid() || !gps.location.isUpdated() || gps.hdop.value() == 0.0 || gps.satellites.value() < 3) {
      while (Serial1.available() > 0) {
        gps.encode(Serial1.read());
      }
    }
    initialAlt = gps.altitude.meters();
    initialLat = gps.location.lat();
    initialLng = gps.location.lng();
    currentDistance = 0.0;
    
    #ifdef SERIAL_MONITOR
      Serial.print("Initial altitude = ");
      Serial.print(initialAlt, 3);
      Serial.print(" Satellites = ");
      Serial.print(gps.satellites.value());
      Serial.print(" hdop = ");
      Serial.println((double) gps.hdop.value() / 100.0);
    #endif
}

void preflightControlCheck() {

  #ifdef SERIAL_MONITOR
    Serial.println("Preflight control surface check.");
  #endif
  
  setPitchAndRoll(0.0,0.0);
}

/*
 * Mimics stick movements used by radio control
 * operator to arm electronic speed control.
 */
void armESC() {

  #ifdef SERIAL_MONITOR
    Serial.print("Arming ESC...");
  #endif
  
  esc.writeMicroseconds(ESC_MIN_MICROSECONDS);
  delay(2000);
  esc.writeMicroseconds(ESC_MAX_MICROSECONDS);
  delay(2000);
  esc.writeMicroseconds(ESC_MIN_MICROSECONDS);
  delay(2000);
  setThrottle(0.0);
}

/*
 * this method is the one and only way to
 * update sensor readings.
 */
void update() {
  accel.getEvent(&event);
  correctedPitch = event.acceleration.y - initialPitch;
  // accelerometer is opposite expected sign for X (for this plane)
  correctedRoll = -1.0 * (event.acceleration.x - initialRoll);

  // Do not block! Just read any GPS text that is available.
  while (Serial1.available() > 0) {
    gps.encode(Serial1.read());
  }

  if (gps.location.isValid() && gps.location.isUpdated()) {
    correctedAlt = gps.altitude.meters() - initialAlt;
    currentLat = gps.location.lat();
    currentLng = gps.location.lng();
    currentDistance = gps.distanceBetween(initialLat,initialLng,currentLat,currentLng);
  }
}

/*
 * wait until plane is held nose-up for 1 second,
 * then flash 5 times and continue.
 */
void wait() {
  while (1) {
    update();
    if (event.acceleration.y > 8.0) {
      delay(1000);
      update();
      if (event.acceleration.y > 8.0) {
        break;
      }
    } else {
      flashDelay(100, 1);
    }
  }
  flashDelay(750, 5);
}

void calibratePitch() {

  #ifdef SERIAL_MONITOR
    Serial.println("Calibrate pitch...");
  #endif
  
  int i = 0;
  boolean done = false;
  while (i < 20) {
    update();
    double y = event.acceleration.y;
    if (abs(y) < ACCEL_CALIBRATION_LIMIT) {
      i++; 
    } else {
      i = 0;
      flashDelay(10, 10);
    }
  }
  initialPitch = event.acceleration.y;
  initialPitch = 0.0;

  #ifdef SERIAL_MONITOR
    Serial.print("Initial pitch = ");
    Serial.println(initialPitch, 6);
  #endif
}

void calibrateRoll() {

  #ifdef SERIAL_MONITOR
    Serial.println("Calibrate roll...");
  #endif
  
  int i = 0;
  boolean done = false;
  while (i < 20) {
    update();
    double x = event.acceleration.x;
    if (abs(x) < ACCEL_CALIBRATION_LIMIT) {
      i++; 
    } else {
      i = 0;
      flashDelay(10, 10);
    }
  }
  initialRoll = event.acceleration.x;
  initialRoll = 0.0;
  
  #ifdef SERIAL_MONITOR
    Serial.print("Initial roll = ");
    Serial.println(initialRoll, 6);
  #endif
}

void abortFlight() {
  while (1) {
    digitalWrite(LED_PIN, HIGH);
  }
}

void flashDelay(int frequency, int count) {
  for (int i = 0; i < count; i++) {
    digitalWrite(LED_PIN, HIGH);
    delay(frequency / 2);
    digitalWrite(LED_PIN, LOW);
    delay(frequency / 2);
  }
}

void setTargetAltitude(double val) {
  targetAlt = val > MIN_OPERATIONAL_ALTITUDE ? val : MIN_OPERATIONAL_ALTITUDE;
}

double servoSetting(double val) {
  if (val > SERVO_LIMIT_MAX) {
    return SERVO_LIMIT_MAX;
  } else if (val < SERVO_LIMIT_MIN) {
    return SERVO_LIMIT_MIN;
  } else {
    return val;
  }
}

/*
 * range is +100 (full upward) to -100 (full downward)
 */
void setLeftElevon(double val) {
  val += LEFT_ELEVON_CORRECTION;
  val = val >  100.0 ?  100.0 : val;
  val = val < -100.0 ? -100.0 : val; 
  double setting = 40.0 + (( val + 100.0 ) / 200.0) * 100.0;
  setting = 180.0 - setting;
  leftElevon.write(servoSetting(setting));
}

/*
 * range is +100 (full upward) to -100 (full downward)
 */
void setRightElevon(double val) {
  val += RIGHT_ELEVON_CORRECTION;
  val = val >  100.0 ?  100.0 : val;
  val = val < -100.0 ? -100.0 : val;  
  double setting = 40.0 + (( val + 100.0 ) / 200.0) * 100.0;
  rightElevon.write(servoSetting(setting));
}

/*
 * both ranges are +100 to -100
 * for pitch: +100 is full climb, -100 is full dive
 * for roll:  +100 is roll hard left, -100 is roll hard right
 */
void setPitchAndRoll(double pitchRequest, double rollRequest) {

  if (pitchRequest > pitchSetting) pitchSetting += 2.0;
  else if (pitchRequest < pitchSetting) pitchSetting -= 2.0;
  
  if (rollRequest > rollSetting) rollSetting += 2.0;
  else if (rollRequest < rollSetting) rollSetting -= 2.0; 

  pitchSetting = pitchSetting >  100.0 ?  100.0 : pitchSetting;
  pitchSetting = pitchSetting < -100.0 ? -100.0 : pitchSetting;

  rollSetting = rollSetting >  100.0 ?  100.0 : rollSetting;
  rollSetting = rollSetting < -100.0 ? -100.0 : rollSetting;

  // compute superposition
  double left = 0.0;
  double right = 0.0;
  left += pitchSetting;
  right += pitchSetting;
  left += rollSetting;
  right -= rollSetting;

  setLeftElevon(left);
  setRightElevon(right);
}

// valid settings are 0 to 100
void setThrottle(double val) {

  val = val > 100.0 ? 100.0 : val; // for testing, use lower limit
  val = val < 0.0 ? 0.0 : val;

  int setting = ESC_MIN_MICROSECONDS + (int) (1000.0 * val / 100.0);
  setting = setting > ESC_MAX_MICROSECONDS ? ESC_MAX_MICROSECONDS : setting;
  setting = setting < ESC_MIN_MICROSECONDS ? ESC_MIN_MICROSECONDS : setting;
  esc.writeMicroseconds(setting);
}

void loop(void) {
  
  // read sensors and update global state variables
  update();

  // target settings
  double pitch;
  double roll;
  double throttle;

  takeoff &= millis() < startTime + TIME_LIMIT_TAKEOFF;

  //enforce failsafes -- if maximum flight time or distance exceeded, land
  landing |= ( millis() > startTime + TIME_LIMIT_MILLISECONDS );
  //landing |= (currentDistance > DISTANCE_LIMIT_METERS);
  
  // update target values
  if (landing) {
    pitch = -1.0 * correctedPitch * 10.0;
    roll = correctedRoll * 10.0;  // target roll is zero
    if (millis() > startTime + TIME_LIMIT_MILLISECONDS + TIME_LIMIT_LANDING) {
      digitalWrite(LED_PIN, HIGH);
      throttle = 0.0;
      pitch += 100.0; 
    } else {
      throttle = 50.0;
      pitch += 15.0; 
    }
  } else if (takeoff) {
    pitch = 12.5;
    roll = 0.0;//TEMP correctedRoll * 10.0;              // target roll is zero
    throttle = 80.0;
  } else {
    // fly the plane  
    // climb
    double deltaPitch = 2.0 - correctedPitch;
    pitch = deltaPitch * 10.0;
    roll = correctedRoll * 10.0;              // target roll is zero
    throttle = 60.0;
  }

  #ifdef SERIAL_MONITOR
   // if (millis() > monitorTime + TIME_LIMIT_MONITOR) {
      Serial.print(throttle, 3);
      Serial.print(" ");
      Serial.print(pitch, 3);
      Serial.print(" ");
      Serial.print(roll, 3);
      Serial.print(" ");
      Serial.print(millis() - startTime);
      Serial.print(" ");
      Serial.print(takeoff);
      Serial.print(" ");
      Serial.print(landing);
      Serial.print(" ");
      Serial.println(currentDistance, 6);
    //  monitorTime = millis();
  //  }
  #endif
  
  setThrottle(throttle);
  setPitchAndRoll(pitch,roll);
}