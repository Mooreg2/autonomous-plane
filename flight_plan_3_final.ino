/*
 * Flight plan, day 3. Unlimited flight; loiter within
 * a specified radius of initial latitude & longitude.
 * 
 * NOTE: this flight plan is intended to be flow offshore
 * and plane will be lost upon landing.
 */

#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Servo.h>
#include <TinyGPS++.h>

//#define SERIAL_MONITOR 1

//pin assignments
const int LED_PIN = 13;
const int ESC_PIN = 9;
const int ELEVATOR_PIN = 10;
const int RUDDER_PIN = 11;

//sensors
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);
TinyGPSPlus gps;

//servos (esc = electronic speed control)
Servo elevator, rudder, esc;

//limits
const int ESC_MIN_MICROSECONDS = 1000;
const int ESC_MAX_MICROSECONDS = 2000;

const double DISTANCE_LIMIT_METERS = 300;
const double MINIMUM_ALTITUDE = 80.0;
const double TARGET_ALTITUDE = 120.0;

const double SERVO_LIMIT_MIN = 40.0;
const double SERVO_LIMIT_MAX = 140.0;
const double SERVO_INTERVAL = (SERVO_LIMIT_MAX - SERVO_LIMIT_MIN); // represents entire motion from center to either extreme
const double SERVO_CENTERED = SERVO_LIMIT_MIN + SERVO_INTERVAL;

/*
 * calibration -- change these values until elevon are even / zero pitch
 * this step is required for each individual plane (settings will differ)
 */
const double RUDDER_CORRECTION = 15.0;
const double ELEVATOR_CORRECTION = 10.0;

//most recent event
sensors_event_t event;

//global state variables -- set once, during pre-flight
long startTime;

double initialAlt;
double initialLat;
double initialLng;

double currentPitch;
double currentRoll;
double correctedAlt;
double currentLat;
double currentLng;
double priorLat;
double priorLng;
double currentDistance;
double targetAlt;
double courseToTarget;
double courseTraveled;

double pitchSetting;
double rudderSetting;

boolean climb;
boolean gpsMonitor;

void setup() {

  #ifdef SERIAL_MONITOR
    Serial.begin(9600);
    while (!Serial);
    Serial.println("autonmous aircraft 1.0");
  #endif
  
  Serial1.begin(9600); //connection for GPS (using Feather + Feather Wing GPS)
  
  pinMode(LED_PIN, OUTPUT);  
  elevator.attach(ELEVATOR_PIN);
  rudder.attach(RUDDER_PIN);
  esc.attach(ESC_PIN);

  //pre-emptive setting in case ESC is armed
  setThrottle(0);
  pitchSetting = 0.0;
  rudderSetting = 0.0;

  // countdown
  flashDelay(750, 10);
  
  // initialize and test all subsystems
  initializeAccelerometer();
  initializeGPS();

  //start interupt that reads GSP serial buffer
  OCR0A = 0xAF;
  TIMSK0 |= _BV(OCIE0A);

  wait();
  preflightControlCheck();  // leaves control surfaces in centered positions
  wait();
  armESC();

  // countdown
  flashDelay(750, 10);
  
  // trap start time
  startTime = millis();

  climb = true;
}

void initializeAccelerometer() {
  if (!accel.begin()) {
    #ifdef SERIAL_MONITOR
      Serial.println("Accelerometer is missing or disconnected.");
    #endif
    abortFlight();
  }
}

void initializeGPS() {
    
    #ifdef SERIAL_MONITOR
      Serial.println("Initializing GPS");
    #endif
    
    while (!gps.location.isValid() || !gps.location.isUpdated() || gps.hdop.value() == 0.0 || gps.satellites.value() < 3) {
      while (Serial1.available() > 0) {
        gps.encode(Serial1.read());
      }
    }
    initialAlt = gps.altitude.meters();
    initialLat = gps.location.lat();
    initialLng = gps.location.lng();
    priorLat = initialLat;
    priorLng = initialLng;
    currentDistance = 0.0;
    
    #ifdef SERIAL_MONITOR
      Serial.print("Initial altitude = ");
      Serial.print(initialAlt, 3);
      Serial.print(" Satellites = ");
      Serial.print(gps.satellites.value());
      Serial.print(" hdop = ");
      Serial.println((double) gps.hdop.value() / 100.0);
      gpsMonitor = true;
    #endif
}

// Executes when interupt is called
SIGNAL(TIMER0_COMPA_vect) {
  // Read any GPS text that is available.
  while (Serial1.available() > 0) {
    gps.encode(Serial1.read());
  }
}

void preflightControlCheck() {

  #ifdef SERIAL_MONITOR
    Serial.println("Preflight control surface check.");
  #endif
  
  setPitch(100, true);
  delay(1000);
  setPitch(0, true);
  delay(1000);
  setPitch(-100, true);
  delay(1000);
  setPitch(0, true);
  delay(1000);
  setRudder(100, true);
  delay(1000);
  setRudder(0, true);
  delay(1000);
  setRudder(-100, true);
  delay(1000);
  setRudder(0, true);
  delay(1000); 
}

/*
 * Mimics stick movements used by radio control
 * operator to arm electronic speed control.
 */
void armESC() {

  #ifdef SERIAL_MONITOR
    Serial.print("Arming ESC...");
  #endif
  
  esc.writeMicroseconds(ESC_MIN_MICROSECONDS);
  delay(2000);
  esc.writeMicroseconds(ESC_MAX_MICROSECONDS);
  delay(2000);
  esc.writeMicroseconds(ESC_MIN_MICROSECONDS);
  delay(2000);
  setThrottle(0.0);
}

/*
 * this method is the one and only way to
 * update sensor readings.
 */
void update() {
  accel.getEvent(&event);
  currentPitch = event.acceleration.y;
  
  // accelerometer is opposite expected sign for X (for this plane)
  currentRoll = -1.0 * event.acceleration.x;

  if (gps.location.isValid() && gps.location.isUpdated()) {
    correctedAlt = gps.altitude.meters() - initialAlt;
    currentLat = gps.location.lat();
    currentLng = gps.location.lng();
    if (priorLat != currentLat || priorLng != currentLng) {
      currentDistance = gps.distanceBetween(initialLat,initialLng,currentLat,currentLng);
      courseToTarget = gps.courseTo(currentLat,currentLng,initialLat,initialLng);
      courseTraveled = gps.courseTo(priorLat,priorLng,currentLat,currentLng);
      priorLat = currentLat;
      priorLng = currentLng;
    }
    gpsMonitor = true;
  }
}

/*
 * wait until plane is held nose-up for 1 second,
 * then flash 5 times and continue.
 */
void wait() {
  while (1) {
    update();
    if (event.acceleration.y > 8.0) {
      delay(1000);
      update();
      if (event.acceleration.y > 8.0) {
        break;
      }
    } else {
      flashDelay(100, 1);
    }
  }
  flashDelay(750, 5);
}

void abortFlight() {
  while (1) {
    digitalWrite(LED_PIN, HIGH);
  }
}

void flashDelay(int frequency, int count) {
  for (int i = 0; i < count; i++) {
    digitalWrite(LED_PIN, HIGH);
    delay(frequency / 2);
    digitalWrite(LED_PIN, LOW);
    delay(frequency / 2);
  }
}

double servoSetting(double val) {
  if (val > SERVO_LIMIT_MAX) {         
    return SERVO_LIMIT_MAX;
  } else if (val < SERVO_LIMIT_MIN) {  
    return SERVO_LIMIT_MIN;
  } else {
    return val;
  }
}

double turnSetting(double val) {
  val = ( val > 90.0 ) ? 90.0 : val;
  return 60.0 * val / 90.0;
}

// valid settings are 0.0 to 100.0
void setThrottle(double val) {

  val = val > 100.0 ? 100.0 : val;
  val = val < 0.0 ? 0.0 : val;

  #ifdef SERIAL_MONITOR
    val = 0.0;
  #endif

  int setting = ESC_MIN_MICROSECONDS + (int) (1000.0 * val / 100.0);
  setting = setting > ESC_MAX_MICROSECONDS ? ESC_MAX_MICROSECONDS : setting;
  setting = setting < ESC_MIN_MICROSECONDS ? ESC_MIN_MICROSECONDS : setting;
  esc.writeMicroseconds(setting);
}

// valid settings are -100 to 100
void setPitch(double val, boolean test) {
  val *= -1.0;
  val = val >  60.0 ?  60.0 : val;
  val = val < -60.0 ? -60.0 : val;

  if (test) {
    pitchSetting = val;
  } else {
    if (val > pitchSetting) pitchSetting += 2.0;
    else if (val < pitchSetting) pitchSetting -= 2.0;
  }

  double setting = SERVO_LIMIT_MIN + (( pitchSetting + 100.0 ) / 200.0) * SERVO_INTERVAL;
  elevator.write(servoSetting(setting) + ELEVATOR_CORRECTION);
}

// valid settings are -100 to 100 (positive = left turn)
void setRudder(double val, boolean test) {
  val *= -1.0;
  val = val >  80.0 ?  80.0 : val;
  val = val < -80.0 ? -80.0 : val;

  if (test) {
    rudderSetting = val;
  } else {
    if (val > rudderSetting) rudderSetting += 2.0;
    else if (val < rudderSetting) rudderSetting -= 2.0;
  }

  double setting = SERVO_LIMIT_MIN + (( rudderSetting + 100.0 ) / 200.0) * SERVO_INTERVAL;     
  rudder.write(servoSetting(setting) + RUDDER_CORRECTION);
}

void loop(void) {
  
  // read sensors and update global state variables
  update();

  // target settings
  double pitch = 0.0;
  double rudder = 0.0;
  double throttle = 0.0;
  double turn = 0.0;
  
  climb |= ( correctedAlt < MINIMUM_ALTITUDE );

  if (climb) {
    pitch = 12.5;
    throttle = 80.0;
    rudder = 0.0;
    climb &= ( correctedAlt < TARGET_ALTITUDE );
  } else {
   /*
    * Set pitch and throttle
    */
    if (correctedAlt > TARGET_ALTITUDE + 50.0) {
      // plane is too high... nose up, slow down
      pitch = -1.0 * currentPitch * 10.0 + 10.0;
      throttle = 30.0;
    } else {
      // plane is within cruising interval
      pitch = -1.0 * currentPitch * 10.0 + 2.0;
      throttle = 70.0;
    }
   /*
    * Set rudder
    */
    if (currentDistance > DISTANCE_LIMIT_METERS) {
      // fly in direction of original lat & lng
      // navigational code is taken from Leo's autonmous kayak :) 
      double a = 360.0 - courseTraveled + courseToTarget;
      double turn_right = a >= 360.0 ? a - 360.0 : a;
      double turn_left = 360.0 - turn_right;

      if (turn_right < 5.0 && turn_left < 5.0) {
        // course is close enough to correct... fly straight
        rudder = 0.0;
      } else if (turn_right < turn_left) {
        // make a right turn (correction to the right is smaller than to the left)
        rudder = -1.0 * turnSetting(turn_right);
      } else {
        // make a left turn (correction to the left is smaller that to the right)
        rudder = turnSetting(turn_left);
      }
    } else {
      // fly onward
      rudder = 0.0;
    }
  }

  setThrottle(throttle);
  setPitch(pitch, false);
  setRudder(rudder, false);
  
  #ifdef SERIAL_MONITOR
    if (gpsMonitor) {
        gpsMonitor = false;
        Serial.print(gps.hdop.value());
        Serial.print(" ");
        Serial.print(correctedAlt, 6);
        Serial.print(" ");
        Serial.print(currentDistance, 6);
        Serial.print(" ");
        Serial.print(courseToTarget, 6);
        Serial.print(" ");
        Serial.print(courseTraveled, 6);
        Serial.print(" ");
        Serial.print(throttle, 2);
        Serial.print(" ");
        Serial.print(pitch, 2);
        Serial.print(" ");
        Serial.println(rudder, 2);
    }
  #endif
}